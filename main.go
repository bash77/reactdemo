package main

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
	"sort"

	"strconv"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/satori/go.uuid"
)

// UserInvite ...
type UserInvite struct {
	ID       int    `json:"id"`
	UID      string `json:"uid"`
	Quantity int    `json:"quantity"`
}

var (
	userInvites = map[int]*UserInvite{}
	seq         = 1
)

// Template ...
type Template struct {
	templates *template.Template
}

// Render ...
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

// ByID ...
type ByID []*UserInvite

func (a ByID) Len() int           { return len(a) }
func (a ByID) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByID) Less(i, j int) bool { return a[i].ID < a[j].ID }

func main() {
	e := echo.New()

	tmpl := &Template{
		templates: template.Must(template.ParseGlob("views/*.html")),
	}
	e.Renderer = tmpl

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Static("/static", "static")

	e.GET("/", index)
	e.GET("/index", index)
	e.GET("/api/invites", listInvites)
	e.POST("/api/invites/new", createInvite)
	e.GET("/api/invites/:id", getInvite)
	e.PUT("/api/invites/:id", updateInvite)
	e.DELETE("/api/invites/:id", deleteInvite)

	for seq < 4 {
		userInvites[seq] = &UserInvite{ID: seq, UID: generateUID(), Quantity: seq * 2}
		seq++
	}

	fmt.Println("Starting http Server on 9090 port...")
	fmt.Println("Ctrl+C для выхода...")
	e.Logger.Fatal(e.Start(":9090"))
}

func index(c echo.Context) error {
	return c.Render(http.StatusOK, "index.html", nil)
}

func listInvites(c echo.Context) error {
	slice := make([]*UserInvite, 0, len(userInvites))
	for _, value := range userInvites {
		slice = append(slice, value)
	}
	sort.Sort(ByID(slice))
	return c.JSON(http.StatusOK, slice)
}

func getInvite(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, userInvites[id])
}

func createInvite(c echo.Context) error {
	u := &UserInvite{
		ID:  seq,
		UID: generateUID(),
	}
	if err := c.Bind(u); err != nil {
		return err
	}
	userInvites[u.ID] = u
	seq++
	return c.JSON(http.StatusCreated, u)
}

func updateInvite(c echo.Context) error {
	u := new(UserInvite)
	if err := c.Bind(u); err != nil {
		return err
	}
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}
	userInvites[id].Quantity = u.Quantity
	return c.JSON(http.StatusOK, userInvites[id])
}

func deleteInvite(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return err
	}
	delete(userInvites, id)
	return c.NoContent(http.StatusNoContent)
}

func generateUID() string {
	return uuid.NewV4().String()
}
